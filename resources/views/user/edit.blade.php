@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('user.update', $user->id) }}" method="post">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" name="name" placeholder="Enter your name" value="{{ $user->name }}">
                    <p class="text-danger">{{ $errors->first('name') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" name="email" placeholder="Enter your email address" value="{{ $user->email }}">
                    <p class="text-danger">{{ $errors->first('email') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" placeholder="Enter your password">
                    <p class="text-danger">{{ $errors->first('password') }}</p>
                    <p>Leave blank if don't want to change it</p>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-sm">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection