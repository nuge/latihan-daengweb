@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('user.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" name="name" placeholder="Enter your name">
                    <p class="text-danger">{{ $errors->first('name') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" name="email" placeholder="Enter your email address">
                    <p class="text-danger">{{ $errors->first('email') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" placeholder="Enter your password">
                    <p class="text-danger">{{ $errors->first('password') }}</p>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection