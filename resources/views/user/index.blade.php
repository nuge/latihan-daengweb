@extends('layouts.master')

@section('content')
    @card(['title' => 'Management Users'])
        @if (session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
        @endif

        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Registered At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($users as $key => $row)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->email }}</td>
                        <td>{{ $row->created_at }}</td>
                        <td>
                            <form action="{{ route('user.destroy', $row->id) }}" method="post">
                                @csrf
                                @method('DELETE')

                                <a href="{{ route('user.edit', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                <button class="btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" class="text-center">No Data</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    @endcard
@endsection