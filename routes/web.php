<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'article', 'as' => 'article.'], function() {
    Route::get('/create', 'ArticleController@create')->name('create');
    Route::post('/', 'ArticleController@store')->name('store');
    Route::get('/{slug?}/{author?}', 'ArticleController@show')->where('slug', '^[\-a-zA-Z]+$')->name('show');
});
// Route::get('/article/create', 'ArticleController@create');
// Route::resource('user', 'UserController');
// Route::resource('user', 'UserController')->only(['index', 'create']);
Route::resource('user', 'UserController')->except(['show']);