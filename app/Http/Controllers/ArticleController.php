<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function create()
    {
        // $tugas = 10;
        // $uas = 60;
        // return 'Hasilnya adalah: ' . ($tugas + $uas);
    
        $url = route('article.create');
        return 'URL dari name route article.create adalah <strong>' . $url . '</strong>';
    }

    public function store(Request $request)
    {
        return 'Mencoba method post';
    }

    public function show($slug = null, $author = 'Anugrah Sandi')
    {
        //return 'Slug dari artikel ini adalah: <strong>' . $slug . '</strong> & Penulisnya: <strong>' . $author . '</strong>';
    
        $url = route('article.show', [$slug, $author]);
        return 'URL dari name route article.show adalah <strong>' . $url . '</strong>';
    }
}
